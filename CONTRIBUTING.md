# Valtech Paper Round Contributing Guidelines

Please follow the below guidelines while comitting changes to repository. This will help in maintenance.

## Git Commit Guidelines
We have very precise rules over how our git commit messages can be formatted. This leads to **more readable messages** that are easy to follow when looking through the **project history**.  But also, we use the git commit messages to **generate the change log**.

### Commit Message Format
Each commit message consists of a **header**, a **body** and a **footer**.  The header has a special format that includes a **type**, a **scope** and a **subject**:
```bash
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

**Any line of the commit message cannot be longer 100 characters! This allows the message to be easier to read online as well as in various git tools.**

#### Example - All Changes
```bash
git commit -m "docs (readme): Add documentation for explaining the commit message"
git commit -m "refactor: Change other things"
```

#### Example - Closing Issues 
```bash
git commit -m "fix (javascript): Some subject of the bug fix

Long message about the commit message. Describe in as much 
detail as necessary.

Closes #5."
```

### Available Types

Must be one of the following:

* **feature**: A new feature
* **fix**: A bug fix
* **docs**: Documentation only changes
* **style**: Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc)
* **refactor**: A code change that neither fixes a bug or adds a feature
* **test**: Adding missing tests
* **chore**: Changes to the build process or auxiliary tools and libraries such as documentation generation


### Scope
The scope could be anything specifying place of the commit change. For example `script`, `style`, `view`, etc...

### Subject
The subject contains succinct description of the change:

* use the imperative, present tense: "change" not "changed" nor "changes"
* don't capitalize first letter
* no dot (.) at the end

### Body
Just as in the **subject**, use the imperative, present tense: "change" not "changed" nor "changes" The body should include the motivation for the change and contrast this with previous behavior.

### Footer
The footer should contain any information about **Breaking Changes** and is also the place to reference GitHub issues that this commit **Closes**.

## Tool: Git-Changelog

Install `git-changelog` globally

```bash
sudo npm install -g git-changelog
```

## Tagging Your Project

In order to have you project versions correctly displayed on your changelog, try to use this commit message format:

```bash
chore (release): v1.4.0 codename(jaracimrman-existence)
```

In order to do that, you can use git annotated tags:

```bash
git tag -a v1.4.0 -m 'chore (release): v1.4.0 codename(jaracimrman-existence)'
```