﻿using System;
using System.IO;
using TechTalk.SpecFlow;
using Valtech.PaperRound.Models;
using Xunit;

namespace Valtech.PaperRound.Specs
{
    [Binding]
    public class StreetTraversalFeatureSteps
    {
        private StreetReporter reporter;
        private TraversalResult _traversalResult;

        [Given(@"A street specification file with path '(.*)'")]
        public void GivenAStreetSpecificationFileWithPath(string p0)
        {
            reporter = new StreetReporter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, p0));
        }

        [Given(@"I use the '(.*)' strategy")]
        public void GivenIUseThe(string p0)
        {
            if (p0.Equals("WestToEast"))
            {
                _traversalResult = reporter.Traverse(new WestToEastStreetTraverser());
            }

            else if (p0.Equals("NorthToSouth"))
            {
                _traversalResult = reporter.Traverse(new NorthFirstStreetTraverser());
            }
        }

        [Then(@"The number of crossings should be '(.*)'")]
        public void ThenTheNumberOfCrossingsShouldBe(int p0)
        {
            Assert.Equal(p0, _traversalResult.Crossings);
        }

        [Then(@"The traversal should be '(.*)'")]
        public void ThenTheTraversalShouldBe(string p0)
        {
            Assert.Equal(Array.ConvertAll(p0.Split(' '), int.Parse), _traversalResult.TraversalPath);
        }
    }
}
