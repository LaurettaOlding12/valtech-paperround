﻿Feature: Report Feature
	In order to manage street layout and house numbering
	As a town planner
	I want to be able to generate report on a given street

Scenario Outline: For a given specification file
	Given I have a valid specification file at '<filePath>'
	When I generate a report
	Then I should get response of '<response>'
		And I should get total houses on the street count of '<total>'
		And I should get houses on north side equals '<north>'
		And I should get houses on south side equals of '<south>'

	Examples: 
		| filePath                                     | response | total | north | south |
		| ..\\..\\..\\..\\..\\data\\input\\street1.txt | true     | 14    | 8     | 6     |
		| ..\\..\\..\\..\\..\\data\\input\\street2.txt | false    | 0     | 0     | 0     |
