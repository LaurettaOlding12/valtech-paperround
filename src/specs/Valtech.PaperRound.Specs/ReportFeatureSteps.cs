﻿using System;
using System.IO;
using TechTalk.SpecFlow;
using Valtech.PaperRound.Models;
using Xunit;

namespace Valtech.PaperRound.Specs
{
    [Binding]
    public class ReportFeatureSteps
    {
        private StreetReporter reporter;
        private StreetReport report;

        [Given(@"I have a valid specification file at '(.*)'")]
        public void GivenIHaveAValidSpecificationFileAt(string p0)
        {
            reporter = new StreetReporter(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, p0));
        }
        
        [When(@"I generate a report")]
        public void WhenIGenerateAReport()
        {
            report = reporter.GenerateReport();
        }
        
        [Then(@"I should get response of '(.*)'")]
        public void ThenIShouldGetResponseOf(bool p0)
        {
            Assert.Equal(p0, report.IsValidSpecification);
        }
        
        [Then(@"I should get total houses on the street count of '(.*)'")]
        public void ThenIShouldGetTotalHousesOnTheStreetCountOf(int p0)
        {
            Assert.Equal(p0, report.TotalHousesCount);
        }
        
        [Then(@"I should get houses on north side equals '(.*)'")]
        public void ThenIShouldGetHousesOnNorthSideEquals(int p0)
        {
            Assert.Equal(p0, report.NorthHousesCount);
        }
        
        [Then(@"I should get houses on south side equals of '(.*)'")]
        public void ThenIShouldGetHousesOnSouthSideEqualsOf(int p0)
        {
            Assert.Equal(p0, report.SouthHousesCount);
        }
    }
}
