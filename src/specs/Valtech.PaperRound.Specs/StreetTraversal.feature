﻿Feature: Street Traversal Feature
	In order to delivers newspapers to all the houses in a given street
	As a paper boy or girl
	I want to be know for a given strategy the list of houses in order 
		And the number of road crossings


Scenario Outline: Using a Strategy of 'WestToEast'
	Given A street specification file with path '<filePath>'
	And I use the '<strategy>' strategy
	Then The number of crossings should be '<count>'
		And The traversal should be '<traverse>'

	Examples: 
	| filePath                                     | strategy       | count | traverse |
	| ..\\..\\..\\..\\..\\data\\input\\street1.txt | WestToEast     |  8    | 1 2 4 3 6 5 7 8 9 10 12 11 13 15        |
	| ..\\..\\..\\..\\..\\data\\input\\street1.txt | NorthToSouth   |  1    | 1 3 5 7 9 11 13 15 12 10 8 6 4 2        |