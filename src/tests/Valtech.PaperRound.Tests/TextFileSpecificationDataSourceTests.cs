﻿using System;
using System.IO;
using Valtech.PaperRound.Contracts;
using Xunit;

namespace Valtech.PaperRound.Tests
{
    public class TextFileSpecificationDataSourceTests
    {
        private const string EMPTY_FILE_PATH = "";
        private const string MISSING_FILE_PATH = @"\some\path\and\file.txt";
        private const string VALID_FILE_PATH = @"..\..\..\..\..\data\input\street1.txt";

        private readonly SpecificationDataSource _dataSource;
        public TextFileSpecificationDataSourceTests()
        {
            _dataSource = new TextFileSpecificationDataSource();
        }

        [Fact]
        public void EmptyFileTest()
        {
            Assert.ThrowsAny<ArgumentNullException>(() => _dataSource.GetInputData(EMPTY_FILE_PATH));
        }

        [Fact]
        public void MissingFileTest()
        {
            Assert.ThrowsAny<FileNotFoundException>(() => _dataSource.GetInputData(MISSING_FILE_PATH));
        }

        [Fact]
        public void ValidFile()
        {
            var input = _dataSource.GetInputData(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, VALID_FILE_PATH));
            Assert.NotNull(input);
        }
    }
}