﻿using System;
using Valtech.PaperRound.Contracts;
using Xunit;

namespace Valtech.PaperRound.Tests
{
    public class SpaceDelimitedInputParserTests
    {
        private const string EMPTY_INPUT = "";
        private const string INVALID_INPUT = "1 2 b";
        private const string VALID_INPUT = "1 2 3";

        private readonly IInputParser _inputParser;
        public SpaceDelimitedInputParserTests()
        {
            _inputParser = new SpaceDelimitedInputParser();
        }

        [Fact]
        public void EmptyInputString()
        {
            Assert.ThrowsAny<ArgumentNullException>(() => _inputParser.Parse(EMPTY_INPUT));
        }

        [Fact]
        public void InvalidInputString()
        {
            Assert.ThrowsAny<ArgumentException>(() => _inputParser.Parse(INVALID_INPUT));
        }

        [Fact]
        public void ValidInputString()
        {
            var parsedInput = _inputParser.Parse(VALID_INPUT);

            Assert.NotEmpty(parsedInput);
        }
    }
}