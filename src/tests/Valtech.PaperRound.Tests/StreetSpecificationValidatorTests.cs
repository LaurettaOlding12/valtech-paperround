﻿using Valtech.PaperRound.Contracts;
using Xunit;

namespace Valtech.PaperRound.Tests
{
    public class StreetSpecificationValidatorTests
    {
        private static readonly int[] _doesNotBeginWithOne = {2, 3, 1};
        private static readonly int[] _containsDuplicates = { 1, 3, 1 };
        private static readonly int[] _missingNumbers = { 1, 3, 4 };
        private static readonly int[] _ValidNumbers = { 1, 3, 2 };

        private readonly IStreetSpecificationValidator _validator;
        public StreetSpecificationValidatorTests()
        {
            _validator = new StreetSpecificationValidator();
        }

        [Fact]
        public void InvalidInput_DoesNotStartWithOne()
        {
            var isValid = _validator.IsValid(_doesNotBeginWithOne);
            Assert.False(isValid);
        }

        [Fact]
        public void InvalidInput_ContainsDuplicates()
        {
            var isValid = _validator.IsValid(_containsDuplicates);
            Assert.False(isValid);
        }

        [Fact]
        public void InvalidInput_MissingNumbers()
        {
            var isValid = _validator.IsValid(_missingNumbers);
            Assert.False(isValid);
        }

        [Fact]
        public void ValidInput_ValidNumbers()
        {
            var isValid = _validator.IsValid(_ValidNumbers);
            Assert.True(isValid);
        }
    }
}