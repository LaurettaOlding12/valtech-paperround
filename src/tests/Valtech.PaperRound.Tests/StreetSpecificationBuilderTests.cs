﻿using System;
using Valtech.PaperRound.Contracts;
using Xunit;

namespace Valtech.PaperRound.Tests
{
    public class StreetSpecificationBuilderTests
    {
        private readonly StreetSpecificationBuilder _builderWithValidValidator;
        private readonly StreetSpecificationBuilder _builderWithInValidValidator;

        public StreetSpecificationBuilderTests()
        {
            _builderWithValidValidator = new StreetSpecificationBuilder(new MoqValidValidator());
            _builderWithInValidValidator = new StreetSpecificationBuilder(new MoqInValidValidator());
        }

        [Fact]
        public void BuilderReturnsSpecification_ForValidInput()
        {
            var specification = _builderWithValidValidator.GenerStreetSpecification(new[] {1, 3, 2});
            Assert.NotNull(specification);
            Assert.Equal(2, specification.NorthHouses.Count);
            Assert.Equal(1, specification.SouthHouses.Count);
        }

        [Fact]
        public void BuilderThrowsException_ForInValidInput()
        {
            Assert.ThrowsAny<InvalidOperationException>(
                () => _builderWithInValidValidator.GenerStreetSpecification(new[] {1, 3, 2}));
        }
    }

    public class MoqValidValidator : IStreetSpecificationValidator
    {
        public bool IsValid(int[] input)
        {
            return true;
        }
    }

    public class MoqInValidValidator : IStreetSpecificationValidator
    {
        public bool IsValid(int[] input)
        {
            return false;
        }
    }
}