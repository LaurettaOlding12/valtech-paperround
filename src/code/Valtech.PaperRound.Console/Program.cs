﻿using System;
using System.IO;

namespace Valtech.PaperRound.Console
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            var inputReader = new TextFileSpecificationDataSource();
            var input = inputReader.GetInputData(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\..\..\..\data\input\street1.txt"));
            var inputParser = new SpaceDelimitedInputParser();
            var listOfHouses = inputParser.Parse(input);
            var validator = new StreetSpecificationValidator();
            var builder = new StreetSpecificationBuilder(validator);

            var specification = builder.GenerStreetSpecification(listOfHouses);

            
            System.Console.Write($"Number of Houses in the Street: {specification.NorthHouses.Count + specification.SouthHouses.Count}");
            System.Console.Write($"Number of Houses on the North side of the Street: {specification.NorthHouses.Count}");
            System.Console.Write($"Number of Houses on the South side of the Street: {specification.SouthHouses.Count}");
            System.Console.Read();
        }
    }
}