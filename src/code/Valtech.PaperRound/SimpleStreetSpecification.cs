﻿using System.Collections.Generic;
using Valtech.PaperRound.Contracts;
using Valtech.PaperRound.Models;

namespace Valtech.PaperRound
{
    public class SimpleStreetSpecification : IStreetSpecification
    {
        internal SimpleStreetSpecification(IReadOnlyList<int> houses)
        {
            NorthHouses = new List<House>();
            SouthHouses = new List<House>();

            for (var i = 0; i < houses.Count; i++)
            {
                if (houses[i] %2 != 0)
                {
                    NorthHouses.Add(new House(houses[i], i));
                }

                else
                {
                    SouthHouses.Add(new House(houses[i], i));
                }
            }
        }

        public IList<House> NorthHouses { get; }

        public IList<House> SouthHouses { get; }
    }
}