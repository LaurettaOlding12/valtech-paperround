using System;
using System.Collections.Generic;
using System.Linq;
using Valtech.PaperRound.Contracts;
using Valtech.PaperRound.Models;

namespace Valtech.PaperRound
{
    public class WestToEastStreetTraverser : ITraversalStrategy
    {
        public int GetCrossingCount(IStreetSpecification streetSpecification)
        {
            var orderedHouses = GetOrderedHouses(streetSpecification);

            var crossings = 0;

            for (var i = 0; i < orderedHouses.Count - 1; i++)
            {
                if (Math.Abs(orderedHouses[i + 1].Number - orderedHouses[i].Number) != 2)
                {
                    crossings += 1;
                }
            }

            return crossings;
        }

        public int[] GetTraversalPath(IStreetSpecification streetSpecification)
        {
            var orderedHouses = GetOrderedHouses(streetSpecification);
            return orderedHouses.Select(oh => oh.Number).ToArray();
        }

        private IList<House> GetOrderedHouses(IStreetSpecification streetSpecification)
        {
            return streetSpecification.NorthHouses.Concat(streetSpecification.SouthHouses).OrderBy(h => h.Position).ToList();
        }
    }
}