﻿using System.Linq;
using Valtech.PaperRound.Contracts;

namespace Valtech.PaperRound
{
    public class StreetSpecificationValidator : IStreetSpecificationValidator
    {
        public bool IsValid(int[] input)
        {
            if (input[0] != 1)
            {
                return false;
            }

            if (input.Distinct().Count() != input.Length)
            {
                return false;
            }

            var oddNumbers = input.Where(x => x % 2 != 0).OrderBy(x => x).ToArray();

            if ((oddNumbers.Last() + 1) / 2 != oddNumbers.Length)
            {
                return false;
            }
            var evenNumbers = input.Where(x => x % 2 == 0).OrderBy(x => x).ToArray();

            if (evenNumbers.Last() / 2 != evenNumbers.Length)
            {
                return false;
            }

            return true;
        }
    }
}