using System;
using Valtech.PaperRound.Contracts;

namespace Valtech.PaperRound
{
    public class StreetSpecificationBuilder
    {
        private readonly IStreetSpecificationValidator _validator;
        public StreetSpecificationBuilder(IStreetSpecificationValidator validator)
        {
            _validator = validator;
        }

        public IStreetSpecification GenerStreetSpecification(int[] listOfHouses)
        {
            if (_validator.IsValid(listOfHouses))
            {
                return new SimpleStreetSpecification(listOfHouses);
            }

            throw new InvalidOperationException("Cannot build specification");
        }
    }
}