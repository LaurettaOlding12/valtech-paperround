﻿using System;
using Valtech.PaperRound.Contracts;

namespace Valtech.PaperRound
{
    public class SpaceDelimitedInputParser : IInputParser
    {
        private readonly char _seperator = ' ';
        public int[] Parse(string input)
        {
            if (string.IsNullOrEmpty(input))
            {
                throw new ArgumentNullException(nameof(input), "Cannot generate street specification for empty string");
            }

            try
            {
                return Array.ConvertAll(input.Split(_seperator), int.Parse);
            }

            catch (Exception e)
            {
                throw new ArgumentException(nameof(input), e.Message);
            }
        }
    }
}