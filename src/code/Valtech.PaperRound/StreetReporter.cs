﻿using System;
using Valtech.PaperRound.Contracts;
using Valtech.PaperRound.Models;

namespace Valtech.PaperRound
{
    public class StreetReporter
    {
        private readonly string _filePath;
        private IStreetSpecification _specification;
        private bool _isValidSpecification;
        public StreetReporter(string filePath)
        {
            _filePath = filePath;
            GenerateSpecification();
        }

        private void GenerateSpecification()
        {
            try
            {
                var a = new TextFileSpecificationDataSource();
                var data = a.GetInputData(_filePath);
                var parser = new SpaceDelimitedInputParser();
                var houses = parser.Parse(data);
                var builder = new StreetSpecificationBuilder(new StreetSpecificationValidator());
                _specification = builder.GenerStreetSpecification(houses);
                _isValidSpecification = true;
            }

            catch (Exception)
            {
                _isValidSpecification = false;
            }
        }

        public StreetReport GenerateReport()
        {
            if (_isValidSpecification)
            {
                return new StreetReport
                {
                    IsValidSpecification = true,
                    SouthHousesCount = _specification.SouthHouses.Count,
                    NorthHousesCount = _specification.NorthHouses.Count,
                    TotalHousesCount = _specification.NorthHouses.Count + _specification.SouthHouses.Count
                };
            }

            return new StreetReport
            {
                IsValidSpecification = false
            };
        }

        public TraversalResult Traverse(ITraversalStrategy strategy)
        {
            if (_isValidSpecification)
            {
                return new TraversalResult
                {
                    Crossings = strategy.GetCrossingCount(_specification),
                    TraversalPath = strategy.GetTraversalPath(_specification)
                };
            }

            return null;
        }
    }
}