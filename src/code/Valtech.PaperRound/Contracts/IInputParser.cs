﻿namespace Valtech.PaperRound.Contracts
{
    public interface IInputParser
    {
        int[] Parse(string input);
    }
}