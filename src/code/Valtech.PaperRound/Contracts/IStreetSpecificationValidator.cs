namespace Valtech.PaperRound.Contracts
{
    public interface IStreetSpecificationValidator
    {
        bool IsValid(int[] input);
    }
}