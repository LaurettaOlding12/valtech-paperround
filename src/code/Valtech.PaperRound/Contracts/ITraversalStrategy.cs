namespace Valtech.PaperRound.Contracts
{
    public interface ITraversalStrategy
    {
        int GetCrossingCount(IStreetSpecification streetSpecification);
        int[] GetTraversalPath(IStreetSpecification streetSpecification);
    }
}