﻿namespace Valtech.PaperRound.Contracts
{
    public abstract class SpecificationDataSource
    {
        public abstract string GetInputData(string filePath);
    }
}