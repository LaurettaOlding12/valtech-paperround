﻿using System.Collections.Generic;
using Valtech.PaperRound.Models;

namespace Valtech.PaperRound.Contracts
{
    public interface IStreetSpecification
    {
        IList<House> NorthHouses { get; }
        IList<House> SouthHouses { get; }
    }
}