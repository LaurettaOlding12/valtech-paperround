namespace Valtech.PaperRound.Models
{
    public class StreetReport
    {
        public bool IsValidSpecification { get; set; }
        public int TotalHousesCount { get; set; }
        public int NorthHousesCount { get; set; }
        public int SouthHousesCount { get; set; }
    }
}