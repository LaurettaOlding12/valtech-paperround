namespace Valtech.PaperRound.Models
{
    public class TraversalResult
    {
        public int Crossings { get; set; }
        public int[] TraversalPath { get; set; }
    }
}