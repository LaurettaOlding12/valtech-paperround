﻿namespace Valtech.PaperRound.Models
{
    public class House
    {
        public House(int number, int position)
        {
            Number = number;
            Position = position;
        }
        public int Position { get; }
        public int Number { get; }
    }
}