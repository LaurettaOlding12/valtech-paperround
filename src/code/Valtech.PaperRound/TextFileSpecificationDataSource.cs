﻿using System;
using System.IO;
using Valtech.PaperRound.Contracts;

namespace Valtech.PaperRound
{
    public class TextFileSpecificationDataSource : SpecificationDataSource
    {
        public override string GetInputData(string filePath)
        {
            if (string.IsNullOrEmpty(filePath))
            {
                throw new ArgumentNullException(nameof(filePath), "Path cannot be empty");
            }

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException($"File {filePath} does not exist");
            }

            using (var stream = File.OpenRead(filePath))
            using (var reader = new StreamReader(stream))
            {
                return reader.ReadToEnd();
            }
        }
    }
}