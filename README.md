# Valtech Paper Round Assignment

This project is an assignment for the Paper Round for Valtech Interview for the position of Software Developer.

## About the paper Round

Please implement code to deliver the user stories below as described by their acceptance criteria (ACs).

You should aim to deliver a working solution that is straightforward, maintainable, bug-free, production quality and employing best software development practices. You may find ambiguities in the requirements that in the 'real world' you would clarify with a business analyst. For the purpose of this test, please highlight any assumptions that you have had to make in developing your solution.

You should feel free to use any other tools you are more comfortable with.

## User Stories

* User Story #1:
* User Story #2: 

I hope, user would like to submit bug free and interesting solution stories here.
I am also looking for a bug free software to perform my data keeping tasks at my workplace at Houston ( http://www.shreddinghouston.com/paper-shredding-houston/ ).

## Assumptions and Exclusions

* Because of the nature and scopr of the task, I have decided not to use any mocking framework for my tests by rather provide mock implementation within the tests.
* Haven't used any IoC containers

